<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', '35.232.252.63:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i#oq/0;WI]eMN8)z,1`)1P>?!gqh<&/)Ns=>IMR*S&JHfxJ5)=Y9;Y6*]UgR^5Kn' );
define( 'SECURE_AUTH_KEY',  '&i;852[xQ$h}Ix[5hgdBZ-5}0_yRF.j=l8|7?.%7vm9{$vd_3yS;bA]@<f,i,y9d' );
define( 'LOGGED_IN_KEY',    'M3lFku9G~s@>ed,k}JSqC^j&7FAYI!vK[]hVC7V&^~|F[>{U(J0 Al&h3EgJmwLJ' );
define( 'NONCE_KEY',        'TgSkQfdpGk*vD@?fax}@ZQ4lOssn>?FRBWor-j{W9W)qHUfcLmS_mi9^!HTGv*T/' );
define( 'AUTH_SALT',        'Cwu$A;3~.acd?2?;u1A)z<D^V-vuLlaNTKu>EV>SjKY47<3=e`J]72?_]GWeWZIp' );
define( 'SECURE_AUTH_SALT', 'c):U=,@=Q V)y2M::8`Ljy6}NJj#[.Gs^0i=8=?pP}YmgGA97NXw: Oq^4yZV(}@' );
define( 'LOGGED_IN_SALT',   '#=GwI_A|iC^o.lbYq$<`k;UdGh.RONyZ5TTej/ZuyH(%*!uRMM~8[-C%pk2ZJ9jh' );
define( 'NONCE_SALT',       '/6QoQtrEYlVU.+4/}D,l#p[jo~Fu{-&NG,Q-naQ8+tId9aIy,2d,RM9Iw#|[0<;:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

